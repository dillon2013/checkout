import React from 'react';
import styles from './Form.module.scss';

const initialFormState = {
    name: {value: '', valid: false, pristine: true},
    email: {value: '', valid: false, pristine: true},
    rating: {value: '1', valid: true, pristine: true},
    comment: {value: '', valid: false, pristine: true},
};

class Form extends React.Component {

    state = {
        formFields: initialFormState
    }

    onChangeInput = (e) => {
        const {name, value, validity: {valid}} = e.target;

        this.setState({
            formFields: {
                ...this.state.formFields,
                [name]: { value, valid, pristine: false },
            }
        })
    }

    submitForm = (e) => {
        e.preventDefault();
        
        const formIsNotValid = !e.target.checkValidity();
        if(formIsNotValid) {
            const {name, email, rating, comment} = this.state.formFields;
            this.setState({
                formFields: {
                    name: {...name, pristine: false},
                    email: {...email, pristine: false},
                    rating: {...rating, pristine: false},
                    comment: {...comment, pristine: false},
                }
            })
            return;
        }
        
        const {onSubmit} = this.props;
        let {formFields} = this.state;
 
        const formValues = {
            name: formFields.name.value,
            email: formFields.email.value,
            rating: formFields.rating.value,
            comment: formFields.comment.value,
        }
        onSubmit(formValues)

        this.setState({
            formFields: initialFormState
        })
    }

    render(){
        const {formFields: {name, email, rating, comment}} = this.state;

        const nameNotValid = !name.valid && !name.pristine;
        const emailNotValid = !email.valid && !email.pristine;
        const commentNotValid = !comment.valid && !comment.pristine;

        return (
            <>
                <h2>Comment Form</h2>
                <form onSubmit={this.submitForm} noValidate>
                    <label htmlFor="name" className={styles.label}>
                        Name
                    </label>
                    {nameNotValid && <p className={styles.invalidText}>Name field invalid</p>}
                    <input 
                        type="text" 
                        name="name"
                        id="name" 
                        placeholder="name" 
                        required
                        onChange={this.onChangeInput}
                        value={name.value}
                        className={`${styles.input} ${nameNotValid ? styles.error: ''}`}
                    />

                    <label htmlFor="email" className={styles.label}>
                        Email Address
                    </label>
                    {emailNotValid && <p className={styles.invalidText}>Email field invalid</p>}
                    <input 
                        type="email" 
                        name="email"
                        id="email"
                        placeholder="email" 
                        required
                        onChange={this.onChangeInput}
                        value={email.value}
                        className={`${styles.input} ${emailNotValid ? styles.error: ''}`}
                    />

                    <label htmlFor="rating" className={styles.label}>
                        Rating
                    </label>
                    <select 
                        name="rating" 
                        id="rating"
                        onChange={this.onChangeInput}
                        value={rating.value}
                        className={styles.input}
                    >
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>

                    <label htmlFor="comment" className={styles.label}>Comment</label>
                    {commentNotValid && <p className={styles.invalidText}>Comment field invalid</p>}
                    <textarea 
                        name="comment"
                        id="comment"
                        placeholder="Add comment here"
                        onChange={this.onChangeInput}
                        value={comment.value}
                        required
                        className={`${styles.input} ${styles.textarea} ${commentNotValid ? styles.error: ''}`}
                    />

                    <input type="submit" value="Submit" className={`${styles.input} ${styles.submit} pointer`}/>
                </form>
            </>
        )
    }
}

export {Form};