import React from 'react';
import PropTypes from 'prop-types';
import star from '../icons/icon-star.svg';
import user from '../icons/icon-user.svg';
import styles from './Comment.module.scss';

export {Comment};

function Comment({comment: {name, email, rating, comment}}) {
    const stars = []
    for(var i = 0; i < parseInt(rating); i++ ) {
        stars.push(<img className="star" src={star} alt="star" key={i}/>)
    }

    return (
        <div className={`${styles.container} comment`}>
            <div className={styles.avatarContainer}><img src={user} alt="user"/></div>
            <div className={styles.commentContent}>
                <div className={styles.header}>
                    <a href={`mailto:${email}`} className={styles.name}>{name}</a> 
                    <div className={styles.stars}>{stars}</div>
                </div>
                <div>{comment}</div>
            </div>
        </div>
    )
}

Comment.propTypes = {
    comment: PropTypes.shape({
        name: PropTypes.string,
        email: PropTypes.string,
        rating: PropTypes.string,
        comment: PropTypes.string,
    }).isRequired
}