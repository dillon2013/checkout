import React from 'react';
import PropTypes from 'prop-types';
import {Comment} from './Comment';

export {Comments};

function Comments ({comments}) {
    return (
        <div data-testid="Comments__container">  
            <h2>Comments</h2>
            {comments.map((comment, i) => <Comment comment={comment} key={i} />)}
        </div>
    )
}

Comments.propTypes = {
    comments: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            email: PropTypes.string,
            rating: PropTypes.string,
            comment: PropTypes.string,
        })
    ).isRequired
}