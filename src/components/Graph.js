import React from 'react';
import {Bar} from 'react-chartjs-2';

export {Graph};


function Graph({ratings}) {
    const data = {
        labels: ['1 Star', '2 Stars', '3 Stars', '4 Stars', '5 Stars'],
        datasets: [
          {
            label: 'Comments',
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            data: [ratings['1'], ratings['2'], ratings['3'], ratings['4'], ratings['5']]
          }
        ]
      };

    return (
        <>
            <h2>Comment Graph</h2>
            <Bar 
                data={data}
            />
        </>
        
    )
}