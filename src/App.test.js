import React from 'react';
import { render, fireEvent, within } from '@testing-library/react';
import App from './App';

describe('when on the app', () => {
  let renderedApp;

  beforeEach(() => {
    renderedApp = render(<App />);
  })

  test('should render the comment form title', () => {
    expect(renderedApp.getByText('Comment Form')).toBeInTheDocument();
  });
  
  test('should render the comment graph title', () => {
    expect(renderedApp.getByText('Comment Graph')).toBeInTheDocument();
  });
  
  test('should render the comments title', () => {
    expect(renderedApp.getByText('Comments')).toBeInTheDocument();
  });

  describe('when a form has not been touched', () => {
    test('should not display any error messages', () => {
      expect(renderedApp.queryByText('Name field invalid')).not.toBeInTheDocument();
      expect(renderedApp.queryByText('Email field invalid')).not.toBeInTheDocument();
      expect(renderedApp.queryByText('Comment field invalid')).not.toBeInTheDocument();
    })
  })

  describe('when a form is used incorrectly', () => {
    test('should display an error for invalid name field', () => {
      fireEvent.change(renderedApp.getByLabelText('Name'), {
        target: {value: 'test'}
      })
      fireEvent.change(renderedApp.getByLabelText('Name'), {
        target: {value: ''}
      })
      expect(renderedApp.queryByText('Name field invalid')).toBeInTheDocument()
    })

    test('should display an error for invalid email field', () => {
      fireEvent.change(renderedApp.getByLabelText('Email Address'), {
        target: {value: "foobar"}
      })
      expect(renderedApp.getByText('Email field invalid')).toBeInTheDocument()
    })

    test('should display an error for invalid comment field', () => {
      fireEvent.change(renderedApp.getByLabelText('Comment'), {
        target: {value: 'test'}
      })
      fireEvent.change(renderedApp.getByLabelText('Comment'), {
        target: {value: ''}
      })
      expect(renderedApp.queryByText('Comment field invalid')).toBeInTheDocument()
    })

    test('should display errors when submitting empty fields', () => {
      fireEvent.click(renderedApp.getByText('Submit'));

      expect(renderedApp.getByText('Name field invalid')).toBeInTheDocument();
      expect(renderedApp.getByText('Email field invalid')).toBeInTheDocument();
      expect(renderedApp.getByText('Comment field invalid')).toBeInTheDocument();
    })
  })

  describe('when a form is completed successfully', () => {

    let ratingValue = 5;

    beforeEach(() => {
      fireEvent.change(renderedApp.getByLabelText('Name'), {
        target: {value: 'test name'}
      })
      fireEvent.change(renderedApp.getByLabelText('Email Address'), {
        target: {value: 'foo@bar.com'}
      })
      fireEvent.change(renderedApp.getByLabelText('Rating'), {
        target: {value: ratingValue}
      })
      fireEvent.change(renderedApp.getByLabelText('Comment'), {
        target: {value: 'test comment'}
      })
      fireEvent.click(renderedApp.getByText('Submit'));
    })

    test('should set name field to default value after submitting valid data', () => {
      const defaultValue = '';
      expect(renderedApp.getByLabelText('Name').value).toBe(defaultValue);
    })

    test('should set email field to default value after submitting valid data', () => {
      const defaultValue = '';
      expect(renderedApp.getByLabelText('Email Address').value).toBe(defaultValue);
    })

    test('should set rating field to default value after submitting valid data', () => {
      const defaultValue = '1';
      expect(renderedApp.getByLabelText('Rating').value).toBe(defaultValue);
    })

    test('should set comment field to default value after submitting valid data', () => {
      const defaultValue = '';
      expect(renderedApp.getByLabelText('Comment').value).toBe(defaultValue);
    })
    
    describe('in the comments', () => {
      let commentContainer;
      beforeEach(() => {
        commentContainer = renderedApp.getByTestId('Comments__container');
      })

      test('should display the name in the comment', () => {
        expect(within(commentContainer).getByText('test name')).toBeInTheDocument()
      })
  
      test('should display the comment in the page', () => {
        expect(within(commentContainer).getByText('test comment')).toBeInTheDocument()
      })

      test('should display number of stars that match the rating value', () => {
        const comment = commentContainer.querySelectorAll('.comment')[0];
        const commentStars = comment.querySelectorAll('.star');
        expect(commentStars.length).toEqual(ratingValue);
      })

      test('should link to mail address in name', () => {
        const comment = commentContainer.querySelectorAll('.comment')[0];
        const commentLink = comment.querySelector('a');
        expect(commentLink).toHaveAttribute('href', `mailto:foo@bar.com`)
      })
    })
  })  

})
