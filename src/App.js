import React from 'react';
import { Form } from './components/Form';
import { Comments } from './components/Comments';
import { Graph } from './components/Graph';
import './App.scss'

class App extends React.Component {

  state = {
    comments: [],
    ratings: {
      '1': 0,
      '2': 0,
      '3': 0,
      '4': 0,
      '5': 0,
    }
  }

  onSubmit = (formValues) => {
    const {comments, ratings} = this.state;
    this.setState({
      comments: [ ...comments, formValues],
      ratings: {
        ...ratings,
        [formValues.rating]: ratings[formValues.rating] + 1
      }
    })
  }

  render() {
    const {comments, ratings} = this.state;

    return (
      <div className="container">
        <div className="formGraphContainer">
          <div className="formContainer">
            <Form onSubmit={this.onSubmit} />
          </div>
          <div className="graphContainer">
            <Graph ratings={ratings} />
          </div>
        </div>
        <div className="commentContainer">
          <Comments comments={comments} />
        </div>
        
      </div>
    )
  }
}

export default App;
